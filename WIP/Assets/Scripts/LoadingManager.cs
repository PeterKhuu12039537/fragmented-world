﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class LoadingManager : MonoBehaviour {
	public static LoadingManager instance;

	private int levelCount;
	private CursorLockMode wantedMode;
	void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
			
	}

	// Update is called once per frame
	void Update () {
		char parseInt = ' ';
		if (Input.inputString.Length > 0)
			parseInt = Input.inputString [0];
		if (Char.IsDigit (parseInt)) {
			int chosenScene = int.Parse (parseInt.ToString ()) - 1;
			if (chosenScene == -1)
				chosenScene = 10;
			SceneManager.LoadScene (chosenScene);
		}
	}

	// Apply requested cursor state
	void SetCursorState ()
	{
		Cursor.lockState = wantedMode;
		// Hide cursor when locking
		Cursor.visible = (CursorLockMode.Locked != wantedMode);
	}
	void OnGUI ()
	{
		GUILayout.BeginVertical ();
		// Release cursor on escape keypress
		if (Input.GetKeyDown (KeyCode.Escape))
			Cursor.lockState = wantedMode = CursorLockMode.None;

		switch (Cursor.lockState)
		{
		case CursorLockMode.None:
			GUILayout.Label ("Cursor is normal");
			if (GUILayout.Button ("Lock cursor"))
				wantedMode = CursorLockMode.Locked;
			if (GUILayout.Button ("Confine cursor"))
				wantedMode = CursorLockMode.Confined;
			break;
		case CursorLockMode.Confined:
			GUILayout.Label ("Cursor is confined");
			if (GUILayout.Button ("Lock cursor"))
				wantedMode = CursorLockMode.Locked;
			if (GUILayout.Button ("Release cursor"))
				wantedMode = CursorLockMode.None;
			break;
		case CursorLockMode.Locked:
			GUILayout.Label ("Cursor is locked");
			if (GUILayout.Button ("Unlock cursor"))
				wantedMode = CursorLockMode.None;
			if (GUILayout.Button ("Confine cursor"))
				wantedMode = CursorLockMode.Confined;
			break;
		}

		GUILayout.EndVertical ();

		SetCursorState ();
	}

}
