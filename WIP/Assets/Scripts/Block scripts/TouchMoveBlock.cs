﻿using UnityEngine;
using System.Collections;

public class TouchMoveBlock : MovingBlock {
	public float resetRate;
	public float resetLimit;

	private bool canMove;
	private bool moving;
	private float resetPosCounter;
	public override void Initialise (Transform attachedTransform)
	{
		canMove = false;
		moving = false;
		resetPosCounter = 0;
		if (resetLimit == 0)
			resetLimit = 2;
		if (resetRate == 0)
			resetRate = 1f;
		base.Initialise (attachedTransform);
	}
	public override void PerformAction ()
	{
		if (canMove) {
			resetPosCounter = 0;
			moving = true;
		}
		else
			resetPosCounter += resetRate * Time.deltaTime;
		if (moving)
			MoveBlocksThroughList (false);
		if (resetPosCounter > resetLimit) {
			ReturnToStartPos ();
			ResetLerpCoordinates ();
			ResetCounter ();
			moving = false;
		}
	}

	protected void ReturnToStartPos()
	{
		attachedTransform.position = posList [0];
	}

	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag ("Player"))
			canMove = true;
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player"))
			canMove = false;
	}
}
