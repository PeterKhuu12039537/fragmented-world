﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptStorage: MonoBehaviour {
	public List<GenericBlock> blockBehaviourList;

	private GenericBlock currentScript;	
	// Use this for initialization
	void Start () {
		if (blockBehaviourList.Count == 0) {
			GetComponents (blockBehaviourList);
		}
		foreach (GenericBlock script in blockBehaviourList) {
			script.Initialise (transform);
			if (!script.IsFinished ())
				currentScript = script;
		}
	}
	// Update is called once per frame
	public void Update()
	{
		bool finished = true;
		if (currentScript != null)
			finished = currentScript.IsFinished ();
		if (finished) {
			foreach (GenericBlock script in blockBehaviourList)
				script.PerformAction ();
		} else
			currentScript.PerformAction ();
	}
}
