﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingBlock : GenericBlock {
	public float range;

	protected List<Vector3> posList = new List<Vector3>();

	private bool generatedList;
	private intVector2 toFromBlocks;
	// Use this for initialization
	public override void Initialise (Transform attachedTransform) {	
		generatedList = false;
		ResetLerpCoordinates ();
		SetAttributes (attachedTransform);
		ImplementList ();
	}
	protected void ImplementList ()
	{
		posList.Add (attachedTransform.position);
		ReadPlacedBlocks ();
	}
	private void ReadPlacedBlocks()
	{
		foreach (Transform pointer in attachedTransform.GetComponentsInChildren<Transform> ()) {
			if (pointer.tag == "Marker") {
				posList.Add (pointer.position);
				Destroy (pointer.gameObject);
			}
		}
	}
	protected void GenerateBlockPositionCycle()
	{
		if (!generatedList) {
			List<Vector3> tempList = new List<Vector3> ();
			for (int i = posList.Count - 1; i > 0; i--)
				tempList.Add (posList [i]);		
			posList.AddRange (tempList);
			generatedList = true;
		}
	}
	/// <summary>
	/// Moves the lerp coordinates along the list.
	/// </summary>
	protected void CycleCoordinates()
	{
		int counter = posList.Count - 1;
		toFromBlocks = new intVector2 (toFromBlocks.x + 1 > counter ? 0 : toFromBlocks.x + 1, toFromBlocks.y + 1 > counter ? 0 : toFromBlocks.y + 1);
		ResetCounter ();
	}
	protected void MoveBlocksThroughList(bool cycle)
	{
		bool restartCycle = (toFromBlocks.y < posList.Count - 1 || cycle);
		if (movingCount > 1 + leeWay && restartCycle)
			CycleCoordinates ();
		LerpBlocks (posList[toFromBlocks.x], posList[toFromBlocks.y]);
	}

	protected void ResetLerpCoordinates()
	{
		toFromBlocks = new intVector2 (0, 1);
	}
	public override void PerformAction () {
		GenerateBlockPositionCycle ();
		MoveBlocksThroughList (true);
	}
}
