﻿using UnityEngine;
using System.Collections;

public class ScatterBlock : GenericBlock {
	private Vector3 scatterLocation;
	private Vector3 originLocation;

	private bool assigned;
	/// <summary>
	/// Scatters and repairs the blocks depending on whether resetPos is true or false.
	/// </summary>
	/// <param name="range">Range.</param>
	public override void Initialise (Transform attachedTransform)
	{
		SetAttributes (attachedTransform);
		resetPos = true;
		originLocation = attachedTransform.position;
		assigned = false;
		finished = false;
		if (speed == 0)
			speed = 1;
	}
	public override void PerformAction () {
		if (!assigned) {
			ScatterBlocks (100);
			assigned = true;

		} else
			ScatterRepairBlocks (100);
	}
	protected void ScatterRepairBlocks(float range)
	{
		if (resetPos) {
			if (setPos) {
				if (movingCount == 0) {
					scatterLocation = attachedTransform.position;
					scatterLocation += attachedTransform.up * Random.Range (-range, range);
					scatterLocation += attachedTransform.right * Random.Range (-range, range);
				}
				setPos = false;
				finished = false;
				ResetCounter ();
			} else {
				LerpBlocks (originLocation, scatterLocation);
			}
		} else {
			if (!setPos) {
				setPos = true;
				ResetCounter ();
			} else {
				LerpBlocks (scatterLocation, originLocation);
				finished = ReachedDestination ();
			}
		}

	}
	/// <summary>
	/// Scatters the blocks. Should be used only at the start.
	/// </summary>
	/// <param name="range">Range.</param>
	protected void ScatterBlocks(float range)
	{
		scatterLocation = attachedTransform.position;
		scatterLocation += attachedTransform.up * Random.Range (-range, range);
		scatterLocation += attachedTransform.right * Random.Range (-range, range);
		attachedTransform.position = scatterLocation;
		scatterLocation = attachedTransform.position;
		movingCount = 1;
		setPos = false;
	}
}
