﻿using UnityEngine;
using System.Collections;

public class FallScript : GenericBlock {
	private Vector3 originalPosition;
	// Use this for initialization
	public override void Initialise (Transform attachedTransform) {
		SetAttributes(attachedTransform);
		originalPosition = attachedTransform.position;
		ResetCounter ();
		ResetPos = false;
	}
	
	// Update is called once per frame
	public override void PerformAction () {
		if (ResetPos) {
			AddCount ();
		}
		
		Rigidbody rb = transform.GetComponent<Rigidbody> ();
		if (movingCount > 3) {	
			rb.isKinematic = false;
			rb.useGravity = true;				
			ResetPos = false;
		}
		if (transform.position.y < -100) {
			rb.isKinematic = true;
			rb.useGravity = false;
			transform.position = originalPosition;
			ResetCounter ();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
			ResetPos = true;
	}
}
