﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericBlock : MonoBehaviour
{
	public bool ResetPos { get { return resetPos;} set { resetPos = value; } }

	public float leeWay;
	public float speed;

	protected Transform attachedTransform { get; set; }
	protected float movingCount;
	protected bool resetPos;
	protected bool setPos;
	[SerializeField]
	protected bool finished;

	public abstract void Initialise (Transform attachedTransform);
	public abstract void PerformAction ();

	public bool IsFinished()
	{
		return finished;
	}

	protected void SetAttributes(Transform attachedTransform)
	{
		finished = true;
		this.attachedTransform = attachedTransform;
		setPos = true;
		if (speed == 0)
			speed = 0.25f;
		ResetCounter ();
	}

	protected void LerpBlocks(Vector3 a, Vector3 b)
	{
		attachedTransform.position = Vector3.Lerp (a, b, movingCount);
		AddCount ();
	}

	protected void AddCount()
	{
		movingCount += speed * Time.deltaTime;
	}
	protected void ResetCounter()
	{
		movingCount = 0 - leeWay;
	}

	protected bool ReachedDestination()
	{
		return movingCount >= 1;
	}
		
}

