﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepairScript : MonoBehaviour {
	[SerializeField]
	private List<GenericBlock> blockList;

	// Use this for initialization
	void Start () {
		blockList = new List<GenericBlock> ();
		foreach (GenericBlock currentBlock in transform.parent.gameObject.GetComponentsInChildren<GenericBlock>()) {
			blockList.Add (currentBlock);
		}
	}

	public void RepairBlocks()
	{
		foreach (GenericBlock currentBlock in blockList) {
			print ("hit");
			currentBlock.ResetPos = false;
		}
	}
}
