﻿using System;

public struct intVector2
{
	public int x;
	public int y;

	public intVector2 (int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}


