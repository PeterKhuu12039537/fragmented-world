﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {
	public static CharController instance;
	public float speed = 6.0F;
	public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;

	public int damp;
	public int airDamp;

	private Vector3 moveDirection;
	private Vector2 axisRaw;
	private float terminalVelocity;
	[SerializeField]
	private Vector3 safeBlockPosition;
	private Vector3 neutralAirDirection;
	[SerializeField]
	private bool ground;
	void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			instance.transform.position = gameObject.transform.position;
			Destroy (gameObject);
		}

	}
	void Start()
	{
		moveDirection = Vector3.zero;
		axisRaw = Vector2.zero;
		safeBlockPosition = Vector3.zero;
		neutralAirDirection = Vector3.zero;
		ground = false;
		terminalVelocity = gravity * 5;
	}
	void Update() {
		CharacterController controller = GetComponent<CharacterController>();
		ground = controller.isGrounded;

		terminalVelocity = gravity * 5;
		float airResist = ground ? 0 : jumpSpeed - moveDirection.y;
		airResist = airResist > terminalVelocity ? terminalVelocity : airResist;

		decimal determinedDamp = ground ? (decimal)damp : (decimal)(airDamp *  (1 - (airResist/terminalVelocity)));
		decimal currentDamp = determinedDamp != 0 ? (decimal)(1 / determinedDamp) : 0;

		axisRaw = new Vector2(SetDirection (axisRaw.x, "Horizontal", (float)currentDamp), SetDirection (axisRaw.y, "Vertical", (float)currentDamp));
		//axisRaw /= moveDirection.y / jumpSpeed;

		moveDirection = new Vector3(axisRaw.x * speed, moveDirection.y, axisRaw.y * speed);
		moveDirection = transform.TransformDirection(moveDirection);

		if (ground) {			
			moveDirection.y = 0;
			if (Input.GetButton ("Jump")) {
				moveDirection.y = jumpSpeed;
			}
		} else if (Input.GetAxisRaw ("Horizontal") == 0 && Input.GetAxisRaw ("Vertical") == 0) {
			moveDirection = neutralAirDirection;
		} else
			transform.parent = null;
		if (Mathf.Abs (moveDirection.y) < terminalVelocity)
			moveDirection.y -= gravity * Time.deltaTime;
		else {
			float currentY = transform.position.y;
			transform.position = new Vector3(safeBlockPosition.x, Mathf.Abs(currentY), safeBlockPosition.z);
			axisRaw = Vector2.zero;
		}
		controller.Move((moveDirection) * Time.deltaTime);
		neutralAirDirection = moveDirection;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Fragment")) {
			other.GetComponent<RepairScript> ().RepairBlocks ();
		}
		if (other.CompareTag ("SafepointBlock")) {
			safeBlockPosition = other.transform.position;
		}
		if (other.CompareTag ("MainCamera")) {
			if (other.GetComponent<MouseAimCamera> ().maxCameraRadius <= 1.5) {
				foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>())
					renderer.enabled = false;
			}
		}
		//print ("true");
	}

	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag ("InteractionBlock")) {
			if (ground)
				transform.parent = other.transform;
			else
				transform.parent = null;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("InteractionBlock")) {
			transform.parent = null;
		}
		if (other.CompareTag ("MainCamera")) {
			if (other.GetComponent<MouseAimCamera> ().maxCameraRadius != 0) {
				foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>())
					renderer.enabled = true;
			}
		}
	}
	private float SetDirection(float currentValue, string setDirection, float damp)
	{
		float direction = Input.GetAxisRaw (setDirection);
		float smoothDirection = Input.GetAxis (setDirection);
		//print ("Axis " + smoothDirection + " AxisRaw " + direction);
		if (smoothDirection != 0 && direction == 0 && Input.anyKey)
			direction = 1 * Mathf.Abs (smoothDirection) / smoothDirection;
		float temp = currentValue;
		if (ground) {
			decimal determinedValue = (currentValue != 0 || direction != 0) ?  (decimal) direction -  (decimal) currentValue : 0;
			float determinedDirection = (float)determinedValue;
			if (Mathf.Abs (determinedDirection) <= damp)
				determinedDirection = 0;
			if (direction == 0 && smoothDirection == 0 && determinedDirection == 0)
				temp = 0;
			temp += determinedDirection != 0 ? Mathf.Abs(determinedDirection) / determinedDirection * damp : 0;
		} else {
			temp = currentValue + direction * damp;
		}
		//print (setDirection + " " + temp + " " + damp);
		return Mathf.Abs (temp) > 1 ? 1 * (Mathf.Abs(currentValue) / currentValue) : temp;
	}
}
