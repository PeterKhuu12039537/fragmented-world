﻿using UnityEngine;
using System.Collections;

public class MouseAimCamera : MonoBehaviour {
	public static MouseAimCamera instance;
	public float maxCameraRadius = 12.0F;
	public GameObject target;
	public float rotateSpeed = 5;
	public bool invert;

	private Vector3 offset;
	[SerializeField]
	private float vertical;
	void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}

	}

	void Start() {
		offset = new Vector3(0, 0, maxCameraRadius);
		//offset = target.transform.position - transform.position;
		//print (offset);
	}
	void LateUpdate() {
		if (maxCameraRadius <= 2)
			maxCameraRadius = 0;
		float direction = invert ? 1 : -1;
		offset = new Vector3(0, 0, maxCameraRadius);
		
		float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
		vertical += Input.GetAxis ("Mouse Y") * rotateSpeed * direction;
		if (Mathf.Abs(vertical) > 89)
			vertical = 89 * Mathf.Abs(vertical)/vertical;
		else if (maxCameraRadius != 0 && vertical < -2)
			vertical = -2;
		float desiredAngleX = target.transform.eulerAngles.y;
		target.transform.Rotate (0, horizontal, 0);
		Quaternion rotation = Quaternion.Euler (vertical, desiredAngleX, 0);
		transform.position = target.transform.position - (rotation * offset);
		if (maxCameraRadius != 0) {	
			transform.LookAt (target.transform);
		} else {
			transform.rotation = Quaternion.Euler (vertical, desiredAngleX, 0);
		}
//		if (Mathf.Abs (vertical) > 89)
//			vertical = 89 * Mathf.Abs (vertical) / vertical;

	}
}
